%
% File acl2017.tex
%
%% Based on the style files for ACL-2015, with some improvements
%%  taken from the NAACL-2016 style
%% Based on the style files for ACL-2014, which were, in turn,
%% based on ACL-2013, ACL-2012, ACL-2011, ACL-2010, ACL-IJCNLP-2009,
%% EACL-2009, IJCNLP-2008...
%% Based on the style files for EACL 2006 by 
%%e.agirre@ehu.es or Sergi.Balari@uab.es
%% and that of ACL 08 by Joakim Nivre and Noah Smith

\documentclass[11pt,a4paper]{article}
\usepackage[hyperref]{acl2017}
\usepackage{latexsym}
\usepackage{times}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{url}
\usepackage{latexsym}
\usepackage{amssymb}
\usepackage[T1]{fontenc}
\usepackage[protrusion=true,expansion]{microtype}
\usepackage{url}
\usepackage{algorithm}
\usepackage{algorithmic}

%\aclfinalcopy % Uncomment this line for the final submission
%\def\aclpaperid{***} %  Enter the acl Paper ID here

%\setlength\titlebox{5cm}
% You can expand the titlebox if you need extra space
% to show all the authors. Please do not make the titlebox
% smaller than 5cm (the original size); we will check this
% in the camera-ready version and ask you to change it back.

\newcommand\BibTeX{B{\sc ib}\TeX}

\title{Incremental reinforcement learning models or something like that}

\author{Author \\
  Affiliation / Address line 1 \\
  Affiliation / Address line 2 \\
  Affiliation / Address line 3 \\
  {\tt email@domain}}

\date{}

\begin{document}
\maketitle
\begin{abstract} 
\end{abstract}

\section{Introduction}

Developing incremental dialogue systems has gained much attention in recent times. It is particularly important to make the agent more responsive which is essential for improving the task efficiency and perception by the users \cite{selfridge-EtAl:2013:SIGDIAL,hastie-et-al-sigdial-demo:2013,baumann-schlangen:2013:SIGDIAL,dethlefs-hastie-rieser-lemon:2012,selfridge-EtAl:2012:SIGDIAL2012,devault-sagae-traum-dnd2011,skantze-schlangen-eacl-numbers:2009,schlangen-baumann-atterer:2009, khouzaimi2015optimising, maikeeveagent}. Incrementality in dialogue corresponds to managing the turns (when to grab the floor or give up), predicting the user actions and generating responses often overlapping in order to make the conversation more realistic. This task of developing incremental agents is hard that requires careful engineering undertaking in order to make the agents responsive. One such decision is configuring confidence and time thresholds that determine when an agent is confident enough to make decisions. It is often not clear when the confidence is `enough' in order to make the decisions. Reinforcement learning (RL) has been often used in this context to learn the right combinations of components that help make the agent incremental. 

RL in the context of incrementality in dialogue has been studied as a phenomena where agents make decisions as to when they should interrupt the user (Barge-in), stay silent and/or generate backchannels \cite{dethlefs2012optimising} \cite{khouzaimi2015optimising} \cite{dethlefs2016information} \cite{kim2014inverse}. 

In this work we explore building incremental dialogue policy using  Reinforcement learning (RL). We compare the policies learnt by RL with a more carefully engineered high performance and show that the cases where RL performs better. We use real user data for training the policy and testing the RL. The policy learnt by the RL is compared to a strong baseline which has been shown to perform very efficiently (nearly as well as humans) in the conversation task \cite{maikeeveagent}. 

(Note to self: summarize where there is reference to bidding turns \cite{selfridge-heeman:2010:ACL}. Seems more complex that other works).

\section{RDG-Image game}

In this work we use the RDG-Image (Rapid dialogue game)  dataset \cite{paetzel-racca-devault:2014}. The game is a two player collaborative, time-constrained, incentivized rapid conversational game. The players in the game are assigned the roles of the  director and the matcher. The players each see 8 images in different orders on their screen in a 2X4 grid configuration. In the director's screen one of the images is highlighted in red border (target image) as shown in the Figure \ref{fig:user_dialogue_complexity}. The director has to describe the highlighted image such that the matcher can identify the image as quickly as possible. Once the matcher believes they have made the right selection the director can request the next target image (TI). If the matcher has made the right selection both the players get a point. The game continues until the time limit is reached. The time limit is chosen with the intention of creating time pressure. 

\begin{figure*}
\centering
\includegraphics[width=0.8\textwidth]{../figures/can_do_better_1.png}
\caption{Shows examples where the agent can do better. (Change this figure to look more like last figure with confidence curves) }
\label{fig:compare}
\end{figure*}

\subsection{Data}

The data comes in two flavors, human-human and human-agent game conversation. The human-human data were collected in two separate parts, in-lab \cite{paetzel-racca-devault:2014} and web \cite{manuvinakurikepair}. The human-human gameplay data was used to construct a high performance agent in the matcher's role. The agent Eve playing the role of the matcher was deployed over the web \newcite{manuvinaeveagent} and more data was collected which constitutes human-agent part of the data. Table \ref{tab:data} shows the number of subjects and the sub-dialogues collected. Each target image conversation constitutes a sub-dialogue.

\begin{table}[h]
\begin{center}
\resizebox{\linewidth}{!}{
\begin{tabular}{ l  c  c }
\hline
 Branch & \# users & \# sub-dialogues \\ \hline
 Human-Human lab & 64 & 1485 \\ \hline
 Human-Human web & 196 & 5642 \\ \hline
 Human-Agent web & 175 & 7393 \\ \hline
\end{tabular}
}
\end{center}
\caption{\label{tab:data} The table shows the number of users and number of target-image sub-dialgues used for the study.}
\end{table}

\begin{figure}
\centering
\includegraphics[width=0.98\linewidth]{../figures/user_confidence_curve_1.pdf}
\caption{Shows the confidence curve for the user descriptions for the target image highlighted with red border.}
\label{fig:user_dialogue_complexity}
\end{figure}

\subsection{Baseline}

The baseline agent called Eve was developed using the human-human part of the data (lab and web). The agent operates incrementally conversing with the users in real time. The agent uses kaldi ASR \cite{povey2011kaldi} in incremental mode. The ASR operates incrementally producing hypothesis every 100ms which is fed to the language understanding module (NLU). The NLU module is a Naive Bayes classifier trained on bag-of-words features which is generated using frequency threshold \footnote{frequency \textgreater 5} on unigrams and bigrams.
The NLU assigns a confidence score which is a probability distribution over the 8 images present in the context set. The image with the highest score is chosen as the best hypothesis TI by the agent. 
The policy then decides based on the confidence scores as to when the agent should continue listening (wait) or acknowledge that the agent has either `got it' (Assert-Identified; As-I) or `move on' (Asset-skip; As-S). 
The decision is made based on two different thresholds called Identification-Threshold (IT) and GT (Give up threshold). If the confidence value is equal to or greater than the IT then the agent would use `As-I' dialogue act and if the time consumed for the curret target image description has surpassed GT then the agent says `As-S' dialogue act \footnote{utterance by the agent for As-S is `I don't think I can get that one lets move on and for `As-I' it is `Got it'.}. These values for the IT and GT were generated using exhaustive grid search method where the agent selects the best confidence value such that the agent scores maximum points in the game. For more detailed description please refer to \newcite{maikeeveagent}. Algorithm \ref{alg:policy} shows the policy used in the game. The baseline agent is a very efficient and carefully engineered. The baseline agent in real user study performed quite impressively and scored nearly as much as humans.  

\begin{algorithm}[t]
\caption{\label{alg:policy}Eve's dialogue policy}
\begin{algorithmic}
\label{policy-rule}
\IF {$P^*_t > \mathrm{IT} \; \& \; |\mathrm{filtered}(d_t)|\ge 1$} 
        \STATE Assert-Identified
\ELSIF {$\mathrm{elapsed}(t) < \mathrm{GT}$} 
        \STATE wait (continue listening)
        \ELSE \STATE Request-Skip
\ENDIF 
\end{algorithmic}
\end{algorithm}

\subsection{Room for improvement}

Development of an optimal matcher is a hard problem. Lets consider two descriptions in the game as shown in Figure \ref{fig:user_dialogue_complexity} which were used to identify the TI highlighted in red border. The figure shows the confidence curve for the human target image descriptions, where User 1 description (``this looks like a guinea pig with white spot on its head'') is shown in blue and the NLU assigned confidence curve for the User 2 description 
(``It's uh \ldots It's head \ldots uh \ldots Big brown hamster '') is shown in black. It is often hard to make out where the agent should commit  and assert it's selection. Committing at a relatively confidence value of 0.9 would make one of it's selection wrong. Asserting at 100\% confidence would make it's second selection impossible as the confidence never hits the 100\% mark. An optimal matcher would ideally learn these complex policies on when to commit.  

Though the baseline agent is impressive in its performance there are a few shortcomings. Figure \ref{fig:compare} shows the cases where the agent can do better and potentially score much higher by adopting a more complex strategy. There is a give up threshold (GT) after which the agent gives up as it has learnt that it can score higher in the game overall by giving up on the current target image and trying the next one. However, the agent could learn to potentially learn to wait a little longer depending on the image description progression. The agent could also potentially learn to perform better in the game by not being extremely eager but rather wait before the confidence score stabilizes before committing. The agent could also perform better by not learning hard thresholds but rather a much softer range so that the performance could improve in the game and help score better. 

We also study the insight RL learnt policy provides which were earlier hard to think of and include in the baseline policy. (Ex: is there a combination of time and confidence which is not an effective As-I strategy, i.e not commit for some initial time slices for high confidence values and commit for lower confidence values as the user consumes more time).

\section{Design}

The policy descisions is modeled as a MDP (Markov decision policy), i.e $(S, A, T, R, \gamma)$. $S$ is a set of states that the agent can be at. In this task $S$ is represented by $(C,t)$ features where, $C$ is the confidence score assigned by the NLU ($C \longmapsto {\rm I\!R}; 0 \leq C \leq 1$) and $t$ is the time consumed by the director for describing the current target image ($t \longmapsto {\rm I\!R}; 0 \leq t \leq 45.0$) \footnote{Each round lasts a maximum of 45 seconds}. 
 
We use Reinforcement learning to learn a policy ($\pi$) mapping the state ($S$) to the action ($A$), $\pi : S \rightarrow A$, where $A$ = \{As-I, As-S, WAIT\}, the actions to be performed by the agent maximising the overall reward in the game. The As-I and As-S actions map to the utterance by the agent. WAIT action corresponds to the agent listening to the user. The WAIT action results in sampling the state where the next set $C,t$ values are fetched for the same TI description. The As-I and As-S actions fetch the $C,t$ for the next TI within the same context set. The policy is trained one per context set (Any TI within the context set). The difference between the As-I and As-S action is in the reward assigned as shown below. This is to encourage the agent make the selections and avoid the unnecessary As-S actions. However, the policy also encourages the agent to perform As-S to avoid getting heavy negative penalties in case the wrong images are selected. In the game however, since the agent is engineered to have selected a TI always the points will be assigned for both the actions. 

$R$ corresponds to a simple reward function shown below. 

\[ R =
  \begin{cases}
    +\delta    & \quad \text{if action is WAIT}\\
    +100    & \quad \text{if As-I is right}\\
    -100    & \quad \text{if As-I is wrong}\\
    0    & \quad \text{if action is As-S}\\
  \end{cases}
\]

In this work we use Least square policy iteration (LSPI) \cite{lagoudakis2003least} reinforcement learning (RL) algorithm implemented in BURLAP   
\footnote{http://burlap.cs.brown.edu/} java code library. LSPI is a model free off-policy method that combines value function approximation with linear architectures and approximate policy iteration. LSPI in our work uses State-Action-Reward-State (SARS) transitions sampled randomly from the human interactions. We use Radial basis value function. These radial basis functions distributed across statea space uniformly provide a method to compress the state space and give an approximation as to where a given state is. 
It is important to note that the RL doesn't have complex reward function, state representation engineered to the task and a simulated user model for training and testing. 

Initial experimentation with the Vanilla Q-learning \cite{sutton1998reinforcement} did not yield us good results. The reason being large state space. Binning the features in order to reduce the size of state space also did not help as the policy couldn't distinguish the states in a nuanced manner. Value function approximation helps in these cases by learning from the features independently. One of the early reward functions experimented was positive reward (+1) for the right guess and  initial policies learnt by the agent was that the agent would be overly eager to perform As-I as it could score maximum points by being overly committing with As-I. To prevent the agent from learning a policy that is overly eager to commit high reward and high penalty reward function was the best fit.  

\subsection{Features}

It is important that the policy has the best performing NLU module for a proper functioning. We experimented with different versions of training data for the NLU and found that the human-human data and human-agent data ASR transcriptions provide the best NLU accuracy on the test set. As the transcriptions were present for the human-human data we also tested out the performance of the test set on the NLU trained specifically on the human transcribed text. We found that there were no significant differences in the accuracies of the NLU when it was trained with the human transcription and the ASR transcriptions. It is also true that the conversations that the users have with the agent is different from the conversations human interlocutors have with one another. Hence, we tested if the NLU performance would be better if trained with the data from just the ASR transcriptions from human-agent interaction alone. We found that the accuracy of the NLU on the test data was significantly worse compared to the accuracy obtained from training from HH transcripts. But, when the HA data were combined with the HH data we found that the overall accuracy of the NLU improved. This improvement in the accuracy was found to be significant. 

\begin{table}[h]
\begin{center}
%\resizebox{\linewidth}{!}{
\begin{tabular}{| l | l |}
\hline
 Branch & Accuracy \\ \hline
 HH-ASR & 59.72 \\ \hline
 HH-Transcribed & 58.71 \\ \hline
 HA-ASR & 48.70 \\ \hline
 HH+HA-ASR & 61.89 \\ \hline
\end{tabular}
%}
\end{center}
\caption{\label{tab:nluresults} Results of NLU accuracy on test set for different type of training data.}
\end{table}

The time consumed for the current target image was used as a feature for state representation. The time consumed gives the time passed since the moment the current TI loads. This feature is obtained from the game. 


\section{Results}

\begin{figure}
\centering
\includegraphics[width=0.9\linewidth]{../figures/points_time_consumed_42.png}
\caption{Shows the RL policy scoring significantly higher points than the baseline by investing significantly more time on each target image.}
\label{fig:compare_p_t_42}
\end{figure}

\begin{table*}[h]
\begin{center}
%\resizebox{\linewidth}{!}{
\begin{tabular}{| l | l | l | l | l | l | l | l | l | l | l | l | l | l | l |}
\hline
 & \multicolumn{2}{|c|}{pets}  & \multicolumn{2}{|c|}{zoo} & \multicolumn{2}{|c|}{kitten} & \multicolumn{2}{|c|}{cocktail} & \multicolumn{2}{|c|}{bikes} &\multicolumn{2}{|c|}{yoga} &\multicolumn{2}{|c|}{necklace}\\ \hline
 Version & PPS & P & PPS & P & PPS & P & PPS & P & PPS & P & PPS & P & PPS & P \\ \hline
 Baseline & 0.31 & 37 & 0.46 & 27 & 0.26 & 14 & 0.26 & 23 & 0.25 & 13 & 0.01 & 3  & 0.01 & 4  \\ \hline
 RL agent & 0.36 & 37 & 0.37 & 32 & 0.25 & 16 & 0.29 & 23 & 0.21 & 22 & 0.25 & 13 & 0.24 & 18 \\ \hline
\end{tabular}
%}
\end{center}
\caption{\label{tab:pps} Results of points per second (PPS), points (P) on test set comparing baseline and the RL agent.}
\end{table*}

In this work we use conversations from 10\% of the users from Human-Human and Human-Agent branch as test set. The remaining 90\% of the user interaction data is used as training data for NLU and policy. 

From Figure \ref{fig:compare_p_t_42} we can observe that the RL policy has learnt to score more points than the baseline system. We can also observe that it has learnt to do the same by investing more time. Investing more time corresponds to the decision by longer sequences of `WAIT' actions. It is also important to note that the agent has not decided to wait eternally. This can be observed in the Table \ref{tab:pps}. We can see that the Agent has not lost out on the points scored per second (PPS) metric. The points per second parameter is a measure of how effective the agent is at playing the game which is calculated as a ratio total points scored by the agent divided by the total time consumed. 
 
It is important to observe the differences in the policy learnt by the RL and the baseline policy. 

The RL has learnt two different strategies depending the types of image set. The RL agent has learned to score faster than than the baseline agent (Figure \ref{fig:compare_p_t_11}) when the RL has scored as much points as the baseline. In the other image sets where describing the TI is harder RL has learned to invest more time (Figure \ref{fig:compare_p_t_42}). The agent has learnt to strategize differently for different image sets. While, these strategies were learnt automatically we cannot claim that implementing such a strategy is not possible in the baseline system. We could potentially implement these rules in baseline system as well by reducing the IT in easier image sets and including the timing bounds. The gain in performance of the RL is due to the policies learnt by the RL. Figure \ref{fig:compare_policies} shows the difference in policies learnt by the baseline and the RL. We can see that the RL has learnt to seldom commit to perform guess (As-I) when the time consumed is lesser. This makes sense on intuitive levels as the initial few utterances wouldn't be stable in an incremental ASR. The RL has learnt to As-I only when the confidence is a high value and at high times. Lesser confidence when the time consumed is high has also been found to be an efficient strategy to score more in the game. Thus by adopting these strategies agent has learnt to score higher. It is also important to note that the RL has not learnt a hard policy in terms of time consumed and the confidence. 

In contrast with the vanilla q-learning (Figure \ref{fig:compare_policies_ql} we can observe that the due to large state-action space the agent has failed to learn a policy. Value function approximation was found to be more effective due to this reason.   

\begin{figure}
\centering
\includegraphics[width=0.98\linewidth]{../figures/lspirbf_vs_baseline_42.png}
\caption{Shows the performance of the LSPI with RBF features compared to the baseline (top) and vanilla q-learning compared to the baseline (bottom).}
\label{fig:compare_policies}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.98\linewidth]{../figures/q-learning-vs-baseline_42.png}
\caption{Shows the performance of the LSPI with RBF features compared to the baseline (top) and vanilla q-learning compared to the baseline (bottom).}
\label{fig:compare_policies_ql}
\end{figure}


\begin{figure*}
\centering
\includegraphics[width=0.98\textwidth]{../figures/6a.png}
\caption{Shows the action taken by the baseline and the RL agent}
\label{fig:example_action}
\end{figure*}

\begin{table}[h]
\begin{center}
\resizebox{\linewidth}{!}{
\begin{tabular}{ l  c  c  c  c}
\hline
 Set & \# Baseline (PPS) & \# RL (PPS) & \# Baseline (UF) & \# RL (UF) \\ \hline
 1 & 0.339 & 0.342 & 0.204 & 0.235 \\ \hline
 2 & 0.266 & 0.235 & 0.204 & 0.277  \\ \hline
 3 & 0.246 & 0.336 & 0.017 & 0.048 \\ \hline
 4 & 0.261 & 0.229 & 0.059 & 0.207 \\ \hline
\end{tabular}
}
\end{center}
\caption{\label{tab:ppsufset} The table shows the pps and uf.}
\end{table}


\section{Discussion \& future work}

- Example showing the cases that have worked well. 

- We could potentially build these policies in the baseline and RL could help us do this in case we need a more hard policy for a dialogue system. 

- It gave us insights into the things that were hard to think about in the beginning

- RL policy learned implicitly the notion of time remaining even though it was not a part of the states or reward function. This is because of the samples of data. 

- It cannot be argued that the engineering an agent to perform well in the task is more disadvantageous as it also takes iteration on designing reward function and getting the right algorithm for the task. 

\bibliography{acl2017}
\bibliographystyle{acl_natbib}

%\appendix

%\section{Supplemental Material}
%\label{sec:supplemental}

\end{document}
