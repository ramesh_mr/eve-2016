#!/bin/bash

infile="$1"

tmpfile=/tmp/foo
cat "$infile" |sed 's/"//g' | sed 's/:/=/g' |sed 's/sequenceid=,//g'|grep '^[0-9]' > $tmpfile

for eveaction in TTSSTATEMENT SCORE TARGET ENDSTREAM STARTSTREAM NTP LASTSCORE
do
  cat $tmpfile | sed "s/A1R0689JPSQ3OF\t$eveaction/eve\t$eveaction/g" > /tmp/bar
  mv /tmp/bar $tmpfile
done

cat $tmpfile | sed 's/A1R0689JPSQ3OF/U/g' | sed 's/ATFATUMUQJ8X/U/g' | sed 's/A1R0689JPSQ3OF/U/g' | sed 's/A3VYXWL5A61498/U/g' | sed 's/A18CXE17TIZ0YQ/U/g' | sed 's/A20CBLK8650TBC/U/g' | sed 's/A1ED3W8YW8RTO5/U/g' |\
  sed 's/eve/E/g' | sed 's/agent/E/g' | egrep -v 'FINAL[^}]*}[[:space:]]*$' | \
  sed 's/TTSSTATEMENTHANDLE/transcription-segment/g' | \
  sed 's/\tFINAL\t/\ttranscription-segment\t/g' > ${infile}.preprocessed



