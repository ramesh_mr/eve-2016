#!/bin/bash

files="$*"

if [ "$files" = "" ] ; then
  files=`ls *.preprocessed`
fi

ls $files| while read file
do
  num=`echo $file|sed 's/.preprocessed//g'`
#  orchestrate.sh -i $file -k -b 39.5 -e 57 -o . -n U,E,overlap,pause,switch -s U=extended:U:transcription-segment -s E=extended:E:transcription-segment -w 1920 -h 1080 -m 10 -f 22 -s overlap=overlap:U:E -s switch=unordered-offset:U:E -s pause=consecutive-agent-offset:transcription-segment:transcription-segment > $num.orchestrate.sh.out
  if [ "$file" = "evtr_medinc.preprocessed" ] ; then
    orchestrate.sh -i $file -b 22.5 -k -o . -n U,E,overlap,pause,switch -s U=extended:U:transcription-segment -s E=extended:E:transcription-segment -w 1920 -h 1080 -m 10 -f 22 -s overlap=overlap:U:E -s switch=unordered-offset:U:E -s pause=consecutive-agent-offset:transcription-segment:transcription-segment > $num.orchestrate.sh.out
  elif [ "$file" = "evtr_noninc_excerpt.preprocessed" ] ; then
    orchestrate.sh -i $file -b 19 -k -o . -n U,E,overlap,pause,switch -s U=extended:U:transcription-segment -s E=extended:E:transcription-segment -w 1920 -h 1080 -m 10 -f 22 -s overlap=overlap:U:E -s switch=unordered-offset:U:E -s pause=consecutive-agent-offset:transcription-segment:transcription-segment > $num.orchestrate.sh.out
  elif [ "$file" = "noninc1.preprocessed" ] ; then
    sh orchestrate.sh -i $file -k -b 14.5 -e 20 -o . -n U,E,overlap,pause,switch -s U=extended:U:transcription-segment -s E=extended:E:transcription-segment -w 1920 -h 1080 -m 10 -f 22 -s overlap=overlap:U:E -s switch=unordered-offset:U:E -s pause=consecutive-agent-offset:transcription-segment:transcription-segment > $num.orchestrate.sh.out
  else
    sh orchestrate.sh -i $file -k -o . -n U,E,overlap,pause,switch -s U=extended:U:transcription-segment -s E=extended:E:transcription-segment -w 1920 -h 1080 -m 10 -f 22 -s overlap=overlap:U:E -s switch=unordered-offset:U:E -s pause=consecutive-agent-offset:transcription-segment:transcription-segment > $num.orchestrate.sh.out
  fi
  echo "$file"
done

