%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dreuw & Deselaer's Poster
% LaTeX Template
% Version 1.0 (11/04/13)
%
% Created by:
% Philippe Dreuw and Thomas Deselaers
% http://www-i6.informatik.rwth-aachen.de/~dreuw/latexbeamerposter.php
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\pdfminorversion=4
\documentclass[final,hyperref={pdfpagelabels=false}]{beamer}

\usepackage[orientation=portrait,size=a0,scale=1.4]{beamerposter} % Use the beamerposter package for laying out the poster with a portrait orientation and an a0 paper size

\usetheme{I6pd2} % Use the I6pd2 theme supplied with this template

\usepackage[english]{babel} % English language/hyphenation

\usepackage{amsmath,amsthm,amssymb,latexsym} % For including math equations, theorems, symbols, etc
\usepackage{algorithm}
\usepackage{algorithmic}

%\usepackage{times}\usefonttheme{professionalfonts}  % Uncomment to use Times as the main font
%\usefonttheme[onlymath]{serif} % Uncomment to use a Serif font within math environments

\boldmath % Use bold for everything within the math environment

\usepackage{booktabs} % Top and bottom rules for tables

\graphicspath{{figures/}} % Location of the graphics files

\usecaptiontemplate{\small\structure{\insertcaptionname~\insertcaptionnumber: }\insertcaption} % A fix for figure numbering

%----------------------------------------------------------------------------------------
%	TITLE SECTION 
%----------------------------------------------------------------------------------------

\title{\huge ``So, which one is it?''\\The effect of alternative incremental architectures\\ \vspace*{0.3cm} in a high-performance game-playing agent} % Poster title

\author{Maike Paetzel, Ramesh Manuvinakurike and David DeVault} % Author(s)

\institute{8paetzel@informatik.uni-hamburg.de, \{manuvinakurike,devault\}@ict.usc.edu} % Institution(s)

%----------------------------------------------------------------------------------------
%	FOOTER TEXT
%----------------------------------------------------------------------------------------

\newcommand{\leftfoot}{This work was supported by the National Science Foundation under Grant No.IIS-1219253 and by the U.S. Army} % Left footer text

\newcommand{\rightfoot}{} % Right footer text

%----------------------------------------------------------------------------------------

\begin{document}

\addtobeamertemplate{block end}{}{\vspace*{2ex}} % White space under blocks

\begin{frame}[t] % The whole poster is enclosed in one beamer frame

\begin{columns}[t] % The whole poster consists of two major columns, each of which can be subdivided further with another \begin{columns} block - the [t] argument aligns each column's content to the top

\begin{column}{.02\textwidth}\end{column} % Empty spacer column

\begin{column}{.465\textwidth} % The first column

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------
            
\begin{block}{Introduction}

\begin{columns} % Subdivide the first main column
\begin{column}{\textwidth} % The first subdivided column within the first main column
This poster introduces Eve, a high-performance agent that plays a fast-paced image matching game in a spoken dialogue with a human partner.
\vspace{0.7cm}
\\ \textbf{Research topics: }
\begin{itemize}
\item When should an incremental system respond? 
\item Does a dialogue system need to be fully incremental? 
\end{itemize}
\end{column}
\end{columns} % End of the subdivision

\begin{columns} % Subdivide the first main column
\begin{column}{.5\textwidth} % The first subdivided column within the first main column
\begin{itemize}
\item How does incrementality affect user perceptions? 
\end{itemize}
\vspace{0.5cm}
\textbf{Features:}
\begin{itemize}
\item Optimization and operation in three different modes of incrementality 
\item Framework to train and evaluate dialogue policies
\item Large user study on the web (125 human participants involved)
\end{itemize}
\end{column}

\begin{column}{.48\textwidth} % The second subdivided column within the first main column
\centering
\begin{figure}
\includegraphics[width=0.9\linewidth]{gameplay2.png}
\caption{Example of a subdialogue with the fully incremental agent. }
\end{figure}
\end{column}
\end{columns} % End of the subdivision

\end{block}

%----------------------------------------------------------------------------------------
%	RDG IMAGE DOMAIN
%----------------------------------------------------------------------------------------

\begin{block}{The RDG-Image Domain}

\begin{columns} % Subdivide the first main column
\begin{column}{.54\textwidth} % The first subdivided column within the first main column
\begin{itemize}
\item Director and Matcher act as a team:
\begin{itemize}
\item Director gives verbal descriptions for target images
\item Matcher tries to identify target image from distractors
\end{itemize}
\item Team scores one point for each correctly matched image
\begin{itemize}
\item incentivized payment of \$0.02/point
\end{itemize}
\end{itemize}
\end{column}

\begin{column}{.43\textwidth} % The second subdivided column within the first main column
\centering
\begin{figure}
\includegraphics[width=0.8\linewidth]{webgame.png}
\caption{Browser interface for director (target image with red border). }
\end{figure}
\end{column}
\end{columns} % End of the subdivision

\begin{itemize}
\item Goal: Score as many points as possible during four game rounds
\begin{itemize}
\item Time limit per round between 45 and 60 seconds
\end{itemize} 
\item Game design ensures speed and accuracy of communication are the only limiting factor to higher scores
\item System was trained on two corpora (lab: N=68, web: N=179)
\begin{itemize}
\item Lab corpus was manually transcribed and annotated with dialogue acts
\item For agent matcher most important dialogue acts are \textit{Assert-Identified} ("Got it!") and \textit{Request-Skip} ("Let's move on, I clicked")
\end{itemize}
\end{itemize}

\end{block}

%----------------------------------------------------------------------------------------
%	Design of the Agent Matcher
%----------------------------------------------------------------------------------------

\begin{block}{Design of the Agent Matcher}

\begin{figure}
\includegraphics[width=0.95\linewidth]{incrementality_versions.jpg}
\caption{Timeline of processing order of modules in the different versions of incrementality.}
\end{figure}

\begin{itemize}
\item Architecture is designed with three incrementality versions:
\begin{itemize}
\item \textbf{Non-incremental} (NonInc): ASR, NLU and policy operated non-incrementally (performance baseline)
\item \textbf{Partially incremental} (PartInc): Incremental ASR; NLU and policy operated non-incrementally
\item \textbf{Fully incremental} (FullInc): ASR, NLU and policy all operated incrementally
\end{itemize}
\item ASR is based on Kaldi and is adapted from the work of (Pl\'{a}tek and  Jur\v{c}\'{i}\v{c}ek, 2014) with a separate VAD based on the Adaptive Multi-Rate (AMR) codec
\end{itemize}
\vspace*{-0.2cm}
\begin{columns} % Subdivide the first main column
\begin{column}{.54\textwidth} % The first subdivided column within the first main column
\begin{itemize}
\item NLU classifies unigrams and bigrams from ASR results using a Naive Bayes classifier (accuracy of 69.15\%)
\item Policy consists of two parameters
\begin{itemize}
\item \textbf{Identification threshold (IT)}: Minimal classifier confidence at which agent performs an \textit{Assert-Identified}
\item \textbf{Give-up threshold (GT)}: Time in seconds after which agent performs a \textit{Request-Skip}
\end{itemize}
\item Action selection algorithm is invoked after new NLU result becomes available
\end{itemize}
\end{column}

\begin{column}{.43\textwidth} % The second subdivided column within the first main column
\centering
\begin{figure}
\includegraphics[width=0.95\linewidth]{algorithm.png}
\caption{Agent's action selection algorithm with $P^*_t = \max_j P(T=i_j|d_t)$ as maximum probability assigned to any image $i$ for description text $d$ at time $t$ and $\mathrm{elapsed}(t)$ as elapsed time spent on current target image up to time $t$.}
\end{figure}
\end{column}
\end{columns} % End of the subdivision

\end{block}

%----------------------------------------------------------------------------------------

\end{column} % End of the first column

\begin{column}{.03\textwidth}\end{column} % Empty spacer column
 
\begin{column}{.465\textwidth} % The second column


%----------------------------------------------------------------------------------------
%	Policy Optimization
%----------------------------------------------------------------------------------------

\begin{block}{Policy Optimization and Simulation-based Results}

\begin{itemize}
\item Agent as eavesdropper: in simulation, let agent explore strategies for when to respond to spoken image descriptions from our human-human corpora
\begin{itemize}
\item If agent commits to a target or requests a skip, time and score is recorded
\item Grid search across all possible IT (step .01) and GT (step 1) values for each combination of image set and incrementality type
\end{itemize}
\end{itemize}

\begin{figure}
\includegraphics[width=0.8\linewidth]{nlu.jpg}
\caption{Excerpt from an image subdialogue from RDG-Image lab corpus, NLU's evolving classification confidence, elapsed time and correctness of the NLU's best guess image.}
\end{figure}

\begin{itemize}
\item Optimized dialogue policy changes as the incrementality type changes
\item Framework allows offline evaluation of policy in terms of points p and p/s
\begin{itemize}
\item Agent is sometimes able to achieve higher p/s than human matchers
\item Simulated p/s decreases as the level of incrementality decreases
\end{itemize}
\item Hypothesis for live interaction: The FullInc agent can score higher than the less incremental versions 
\end{itemize}
\end{block}

%----------------------------------------------------------------------------------------
%	Results
%----------------------------------------------------------------------------------------


\begin{block}{Human-Agent Evaluation}

\begin{itemize}
\item 125 remote participants, recruited on Amazon Mechanical
Turk, who interacted entirely through their web
browsers
\begin{itemize}
\item 50 were paired with each other (25 Human-Human teams) and
25 were paired with each of the FullInc, PartInc, and NonInc agents
\item From self-disclosure of the directors, 50\% were female, all were over 18 (mean
age 31.01, std. 10.13), and all were native English speakers
\end{itemize}
\item \textbf{Results:}
\end{itemize}

\begin{figure}
\includegraphics[width=0.8\linewidth]{questions_combined.pdf}
\caption{Scores and survey responses by condition (means and standard errors). Wilcoxon rank sum test differences indicated by * (p $<$ 0.05), ** (p $<$ 0.005), *** (p $<$ 0.0005).}
\end{figure}
\begin{itemize}
\item The FullInc agent's performance is quite strong:
\begin{itemize}
\item \textbf{Score} comparable to Human-Human performance
\item \textbf{Score}, perceived \textbf{efficiency}, \textbf{satisfaction with the score} and perceived \textbf{understanding of speech} outperforms PartInc and NonInc
\end{itemize}
\item The results underscore the importance of pervasive incremental processing to achieving human-like performance in some dialogue systems
\end{itemize}
\end{block}


%----------------------------------------------------------------------------------------
%	REFERENCES
%----------------------------------------------------------------------------------------

\begin{block}{Our Related Work}
  
\begin{columns} % Subdivide the first main column
\begin{column}{\textwidth} % The first subdivided column within the first main column
\small
$[0]$ Maike Paetzel, David Nicolas Racca and David DeVault. A Multimodal Corpus of Rapid Dialogue Games. \textit{Language Resources and Evaluation Conference (LREC), 2014.} \\
$[1]$ Ramesh Manuvinakurike and David DeVault. Pair Me Up: A Web Framework for Crowd-Sourced Spoken Dialogue Collection. \textit{IWSDS Workshop on Spoken Dialog Systems, 2015.} \\
$[2]$ Ramesh Manuvinakurike, Maike Paetzel and David DeVault. Reducing the Cost of Dialogue System Training and Evaluation with Online, Crowd-Sourced Dialogue Data Collection. \textit{Workshop on the semantics and pragmatics of dialogue (Semdial), 2015.} \\
$[3]$ Maike Paetzel. Exploring the effect of incremental speech processing on dialogue policy performance in a game-playing agent. \textit{Master thesis, 2015.}
\end{column}
\end{columns} % End of the subdivision 
        
%\nocite{*} % Insert publications even if they are not cited in the poster
%\small{\bibliographystyle{unsrt}
%\bibliography{sample}}

\end{block}

%----------------------------------------------------------------------------------------

\end{column} % End of the second column

\begin{column}{.015\textwidth}\end{column} % Empty spacer column

\end{columns} % End of all the columns in the poster

\end{frame} % End of the enclosing frame

\end{document}