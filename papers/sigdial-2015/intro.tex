This paper presents and evaluates a game playing dialogue agent named Eve that
relies on several forms of incremental language processing to achieve
its best performance.  In recent years, the development and adoption
of incremental processing techniques in dialogue systems has continued
to advance, and more-and-more research systems have included some form
of incremental processing; see for example
\cite{selfridge-EtAl:2013:SIGDIAL,hastie-et-al-sigdial-demo:2013,baumann-schlangen:2013:SIGDIAL,dethlefs-hastie-rieser-lemon:2012,selfridge-EtAl:2012:SIGDIAL2012,devault-sagae-traum-dnd2011,skantze-schlangen-eacl-numbers:2009,schlangen-baumann-atterer:2009}.
One compelling high-level motivation for systems builders to incorporate
incremental processing into their systems is to reduce system response
latency \cite{skantze-schlangen-eacl-numbers:2009}.  Recent studies
have also demonstrated user preference of incremental systems over
non-incremental counterparts
\cite{skantze-schlangen-eacl-numbers:2009,Aist}, shown positive effects of
incrementality on user ratings of system efficiency and politeness
\cite{skantze-hjalmarsson:2010:SIGDIAL}, and even shown increases in
the fluency of user speech when appropriate incremental feedback is
provided \cite{gratch-et-al-virtual-rapport:2006}.

Despite this progress, there remain many open questions about the use
of incremental processing in systems.  One important research
direction is to explore and clarify the implications and advantages of
alternative incremental architectures.  Using
pervasive incremental processing in a dialogue system poses a
fundamental challenge to traditional system architectures, which
generally assume turn-level or dialogue act level units of processing
rather than much smaller and higher frequency incremental units
\cite{schlangen-skantze:dnd2011}.  Rather than completely redesigning
their architectures, system builders may be able to gain some of the
advantages of incrementality, such as reduced response
latencies, by incorporating incremental processing in
select system modules such as automatic speech recognition or language
understanding.
%% DD cutting for space For example, incremental processing might be
%% adopted in automatic speech recognition
%% (e.g. \cite{selfridge-iker-heeman-williams:sigdial2011}) and natural
%% language understanding or reference resolution modules
%% (e.g. \cite{kennington-dia-schlangen:iwsds2015,devault-sagae-traum:dnd-accepted-2011}),
%% while other components such as dialogue management, natural language
%% generation, and text-to-speech synthesis continue to operate in a more
%% traditional, non-incremental fashion.  
The extent to which all modules of a dialogue system need to operate incrementally
to achieve specific effects needs further exploration.

Another important research direction is to develop effective 
optimization techniques for dialogue policies in incremental systems.
Incremental dialogue policies may need to make many fine-grained
decisions per second, such as whether to initiate a backchannel or
interruption of a user utterance in progress.  Developing data-driven
approaches to such decision-making may allow us to build more highly
optimized, interactive, and effective systems than are currently
possible \cite{ward-devault-aaai:2015}.  Yet the computational
techniques that can achieve this fine-grained optimization in practice
are not yet clear.  Approaches that use
(Partially Observable) Markov Decision Processes and a reinforcement
learning framework to optimize fine-grained turn-taking control may
ultimately prove effective (see
e.g. \cite{kim-et-al:interspeech2014,selfridge-EtAl:2012:SIGDIAL2012}), 
but optimizing live system interactions in this way remains a challenge.

In this paper, we present a case study of a high-performance
incremental dialogue system that contributes to both of these research
directions.  First, our study investigates the effects of 
increasing levels of incremental processing on the performance and 
user perceptions of an agent that plays a fast-paced game where the value of rapid
decision-making is emphasized.  In a user study involving 125 human
participants, we demonstrate a level of game performance that is broadly 
comparable to the performance of 
live human players.  Only the version of our agent which makes 
maximal use of incremental processing achieves this 
level of performance, along with significantly higher user ratings of
efficiency, understanding of speech, and naturalness of interaction.

Our study also provides a practical approach to the optimization of
dialogue policies for incremental understanding of users' referential 
language in finite domains; see e.g. \cite{schlangen-baumann-atterer:2009}.  Our optimization approach 
delivers a high level of performance for our agent, and offers
insights into how the optimal decision-making policy can vary as the
level of incrementality in system modules is changed.  This supports a
view of incremental policy optimization as a holistic process to be
undertaken in conjunction with overall system design choices.
