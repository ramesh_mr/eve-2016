#!/bin/bash


out=submission-videos

title=sigdial15-between-videos-880x880.png

between=sigdial-between-videos.avi

# create the title video to be inserted between videos

seconds=3
frames=$(($seconds*30))
j=1
imgfile=/tmp/images
rm $imgfile
while [ $j -le $frames ] ;
do
    echo $title >> $imgfile
    j=$((j+1))
done

mencoder mf://@$imgfile -mf w=880:h=880:fps=30:type=png -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell -oac copy -o $between.tmp

avconv -y -ar 44100 -ac 2 -f s16le -i /dev/zero -i $between.tmp -shortest -c:v copy \
-c:a aac -strict experimental $between

# HH: extract first 46 seconds of the video

# the following generates an .avi that doesn't play on windows media player but does play on windows vlc
# mencoder -ss "0" -endpos 46.7 -oac copy -ovc copy all_vids/HH/output3-2.avi -o $out/HH-example.avi
mencoder -ss 0 -endpos 46.7 -srate 44100 -oac pcm -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell -ofps 30 all_vids/HH/output3-2.avi -o $out/HH-example.avi

exit

# FI: 
# need to combine diferrent sample rates

mencoder -srate 44100 -oac pcm -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell $between all_vids/VidsToday/incremental/output4-3-Alternative2.wav.avi $between all_vids/VidsToday/incremental/output4-3-Alternative1.wav.avi $between all_vids/VidsToday/incremental/output4-2-Alternative2.wav.avi -o $out/FI-examples.avi

# 1. all_vids/VidsToday/incremental/output4-3-Alternative2.wav.avi
#  - "the yellow bike... got it" (fast)
# 2. all_vids/VidsToday/incremental/output4-3-Alternative1.wav.avi
#   - "the bike with the white background" ... "got it"
# 3. all_vids/VidsToday/incremental/output4-2-Alternative2.wav.avi
#  - "white curtains [got it] in the background"

# PI 

mencoder -srate 44100 -oac pcm -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell $between all_vids/medinc_ex/output4-4-robot1.wav.avi $between all_vids/medinc_ex/output4-2-christmas3.wav.avi $between all_vids/VidsToday/med_incremental/output1-2-zoo5.wav.avi -o $out/PI-examples.avi

# all_vids/medinc_ex/output4-4-robot1.wav.avi
l#  - "robot sitting down", okay.  agent pretty fast.
# all_vids/medinc_ex/output4-2-christmas3.wav.avi
#  - "christmas tree with gold ribbon" (agent pretty fast here too)
# all_vids/VidsToday/med_incremental/output1-2-zoo5.wav.avi
#  - it's a penguin like [inaudible]... let's move on, i clicked
#    (good to include one where she skips)

# NI

mencoder -srate 44100 -oac pcm -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell $between all_vids/VidsToday/non_incremental/output3-2-Alternative4.wav.avi $between all_vids/VidsToday/non_incremental/output3-2-necklace5.wav.avi $between all_vids/VidsToday/non_incremental/output3-2-necklace7.wav.avi -o $out/NI-examples.avi

# 1. all_vids/VidsToday/non_incremental/output3-2-Alternative4.wav.avi
#  - "this is a burgundy necklace with glass beads ... got it"
# 
# 2. all_vids/VidsToday//non_incremental/output3-2-necklace5.wav.avi
#  - "Red necklace with several rows... got it" (audio glitches)
# 
# 3. all_vids/VidsToday/non_incremental/output3-2-necklace7.wav.avi
#  - "This is a beaded orange necklace ... got it"
