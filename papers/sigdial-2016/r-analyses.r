data_filename = "eve-results.csv"
library(coin)

# Loading the Data from the tmp directory
cat("File to use as input", data_filename, "\n")
cat("Loading the input file ... \n")
expr = read.table(data_filename, header = TRUE, sep = "\t", fill = TRUE, quote = "")
dim(expr) 
cat("Finished. \n")
attach(expr)

sink('analysis-output-eve.txt')

for (i in grep("_", names(expr))){
  if(is.numeric(expr[1,i]))
  {
        cat("----------------------------------------------------------------------------------------------- \n")
        column <- colnames(expr)[i]
        cat(column)
        
        subs <- subset(expr, (expr$condition == "All" | expr$condition == "DT-sep"))
        fit <- wilcox_test(subs[,i] ~ condition, data = subs, distribution = "exact", alternative = "two.sided")
        print(fit)
        cat("\n")
  }
}





sink()
